<?php

declare(strict_types=1);

function debug($var) {
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
    die();
}

function redirect(string $uri) {
    $url = 'http' . ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ? 's://' : '://');
    $url = $url . $_SERVER['HTTP_HOST'] . $uri;

    header('Location: ' . $url);
    exit;
}