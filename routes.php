<?php

declare(strict_types=1);

/**
 * Routes array
 * $pattern => $route
 */
return [
    '/' => 'EntrepreneurController/index/',
    '/entrepreneur/(\d+)' => 'EntrepreneurController/getById/$1',
    '/entrepreneur/add' => 'EntrepreneurController/create/',
    '/entrepreneur/update/' => 'EntrepreneurController/update',
    '/entrepreneur/delete/(\d+)' => 'EntrepreneurController/delete/$1',

    '/salary/(\d+)' => 'SalaryController/index/$1',
    '/salary/add/(\d+)' => 'SalaryController/create/$1',
    '/salary/delete/(\d+)' => 'SalaryController/delete/$1',
];
