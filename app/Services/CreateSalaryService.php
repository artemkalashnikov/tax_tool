<?php

declare(strict_types=1);

namespace App\Services;

use App\DTO\SalaryDTO;
use App\Models\Salary;
use DateTime;

class CreateSalaryService
{
    private MoneyExchangeService $moneyExchangeService;

    public function __construct(MoneyExchangeService $moneyExchangeService)
    {
        $this->moneyExchangeService = $moneyExchangeService;
    }

    public function save(SalaryDTO $request): void
    {
        $USD = $request->getUSD();
        $bonus = $request->getBonus();
        $gift = $request->getGift();
        $date = $request->getIncomingDate() ?? null;
        $finalUSD = $USD + ($USD * $bonus) / 100 + $gift;

        if (is_null($date)) {
            $date = new DateTime();
        } else {
            try {
                $date = new DateTime($date);
            } catch (\Exception $e) {
                echo $e->getMessage();
            }
        }

        $date = $date->format('d.m.Y');
        $rateUSD = $this->moneyExchangeService->getRate('USD', $date);

        $UAH = $finalUSD * $rateUSD;

        $salary = new Salary(
            (int) $request->getId(),
            (int) $request->getEntrepreneurId(),
            (float) $USD,
            (float) $finalUSD,
            (float) $UAH,
            (int) $bonus,
            (float) $gift,
            (string) $date
        );

        $salary->save();
    }
}