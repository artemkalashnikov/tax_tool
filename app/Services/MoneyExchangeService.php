<?php

declare(strict_types=1);

namespace App\Services;

class MoneyExchangeService
{
    private string $apiUrl = 'https://api.privatbank.ua/p24api/exchange_rates?';
    private \Predis\Client $redis;
    private int $expire = 86400; // Redis expire at time 1 day

    public function __construct()
    {
        $this->redis = new \Predis\Client($_ENV['TAX_TOOL_REDIS']);
    }

    public function getRate(string $currency, string $date): float
    {
        $currency = strtoupper($currency);

        if ($this->redis->hexists($date, $currency)) {
            return (float) $this->redis->hget($date, $currency);
        }

        $this->apiUrl .= 'json&date=' . $date;
        $data = json_decode(file_get_contents($this->apiUrl), true);

        if (empty($data['exchangeRate'])) {
            throw new \Error("Error. Privat bank saves currency rate for last 4 years");
        }

        foreach ($data['exchangeRate'] as $item) {
            if (isset($item['currency']) && ($item['currency'] === $currency)) {
                $rate = (float) (intval($item['saleRateNB'] * 10)) / 10;
                $this->redis->hset($date, $currency, (string) $rate);
                $this->redis->expireat($date, $this->expire);

                return $rate;
            }
        }

        throw new \Error("Error. {$currency} rate is not found");
    }
}
