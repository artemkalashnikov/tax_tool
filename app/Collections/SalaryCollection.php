<?php

declare(strict_types=1);

namespace App\Collections;

use App\Collection;
use App\Models\Salary;

class SalaryCollection extends Collection
{
    private int $position = 0;
    private array $items;
    private int $count;

    public function __construct(array $items)
    {
        $this->items = $this->convertToObjects($items);
        $this->count = count($items);
    }

    private function convertToObjects(array $items): array
    {
        foreach ($items as $key => $item) {
            $items[$key] = new Salary(
                (int) $item['id'],
                (int) $item['entrepreneur_id'],
                (float) $item['usd'],
                (float) $item['final_usd'],
                (float) $item['final_uah'],
                (int) $item['bonus'],
                (float) $item['gift'],
                (string) $item['incoming_date'],
            );
        }

        return $items;
    }

    public function add(Salary $object)
    {
        $this->items[$this->count] = $object;
        $this->count += 1;
    }

    public function current(): Salary
    {
        return $this->items[$this->position];
    }

    public function next()
    {
        $this->position += 1;
    }

    public function key(): int
    {
        return $this->position;
    }

    public function valid(): bool
    {
        return isset($this->items[$this->position]);
    }

    public function rewind()
    {
        $this->position = 0;
    }
}