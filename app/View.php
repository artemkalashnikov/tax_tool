<?php

declare(strict_types=1);

namespace App;

class View
{
    public function render(string $filename, array $params = null)
    {
        if (is_array($params)) {
            extract($params);
        }

        return include(__DIR__ . '/views/' . $filename . '.html');
    }
}