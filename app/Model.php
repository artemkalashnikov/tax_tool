<?php

declare(strict_types=1);

namespace App;

abstract class Model
{
    abstract static public function find(int $id);
    abstract public function save();
    abstract public function delete();
}