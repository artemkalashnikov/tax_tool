<?php

declare(strict_types=1);

namespace App;

class Database extends \PDO
{
    public function __construct()
    {
        $dsn = 'mysql:host=' . $_ENV['TAX_TOOL_MYSQL']['host'] .
            ';dbname=' . $_ENV['TAX_TOOL_MYSQL']['dbname'];

        parent::__construct(
            $dsn,
            $_ENV['TAX_TOOL_MYSQL']['user'],
            $_ENV['TAX_TOOL_MYSQL']['password']);
    }
}