<?php

declare(strict_types=1);

namespace App;

class Router
{
    private $routes = [];

    public function __construct(array $routes)
    {
        $this->routes = $routes;
    }

    public function run()
    {
        $uri = $_SERVER['REQUEST_URI'];

        foreach ($this->routes as $pattern => $route) {

            if (preg_match("~^$pattern$~", $uri)) {

                $route = preg_replace("~$pattern~", $route, $uri);
                $partials = explode('/', $route);

                $controller = '\App\Controllers\\' . array_shift($partials);
                $method = array_shift($partials);
                $parameters = $partials;
                unset($partials);

                $controller = new $controller;
                call_user_func_array([$controller, $method], $parameters);

                break;
            }
        }

        header('HTTP/1.1 404 Not Found', true, 404);
    }
}