<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Controller;
use App\Models\Entrepreneur;

class EntrepreneurController extends Controller
{
    public function index()
    {
        $entrepreneurs = Entrepreneur::findAll();

        if (is_null($entrepreneurs)) {
            return $this->noUsers();
        }

        return $this->view->render('index', [
            'title' => 'Список ФЛП',
            'entrepreneurs' => $entrepreneurs,
        ]);
    }

    public function getById(string $id)
    {
        $entrepreneur = Entrepreneur::find((int) $id);

        return $this->view->render('update', [
            'title' => 'Редактирование ФЛП: ' . $entrepreneur->getFullname(),
            'entrepreneur' => $entrepreneur,
        ]);
    }

    public function create()
    {
        $data = $this->getPostData();
        $entrepreneur = new Entrepreneur(
            $data['id'],
            $data['surname'],
            $data['name'],
            $data['middlename'],
            $data['email']
        );

        $entrepreneur->save();

        redirect('/');
    }

    public function update()
    {
        $this->create();
    }

    public function delete(string $id)
    {
        $entrepreneur = Entrepreneur::find((int)$id);
        $entrepreneur->delete();

        redirect('/');
    }

    private function getPostData()
    {
        $data['id'] = (int)$_POST['id'];
        $data['name'] = $_POST['name'];
        $data['surname'] = $_POST['surname'];
        $data['middlename'] = $_POST['middlename'];
        $data['email'] = $_POST['email'];

        return $data;
    }

    private function noUsers()
    {
        return $this->view->render('nousers', [
            'title' => 'Список ФЛП',
            'description' => 'Список ФЛП пуст'
        ]);
    }
}