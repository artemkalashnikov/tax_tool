<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Controller;
use App\DTO\SalaryDTO;
use App\Models\Entrepreneur;
use App\Models\Salary;
use App\Services\CreateSalaryService;
use App\Services\MoneyExchangeService;

class SalaryController extends Controller
{
    public function index(string $id)
    {
        $salaries = Salary::findAll((int) $id);
        $entrepreneur = Entrepreneur::find((int) $id);

        if (is_null($salaries)) {
            return $this->noSalaries($entrepreneur);
        }

        return $this->view->render('salary', [
            'title' => 'Выписка ' . $entrepreneur->getFullname(),
            'entrepreneur' => $entrepreneur,
            'salaries' => $salaries
        ]);
    }

    public function create(string $entrepreneurId)
    {
        $moneyExchangeService = new MoneyExchangeService();
        $salaryService = new CreateSalaryService($moneyExchangeService);
        $request = $this->getPostData();
        $request->setEntrepreneurId((int) $entrepreneurId);
        $salaryService->save($request);

        redirect('/salary/' . $entrepreneurId);
    }

    public function delete(string $id)
    {
        $salary = Salary::find((int) $id);
        $entrepreneurId = $salary->getEntrepreneurId();
        $salary->delete();

        redirect('/salary/' . $entrepreneurId);
    }

    private function getPostData(): SalaryDTO
    {
        return new SalaryDTO(
            (int) $_POST['id'],
            (float) $_POST['usd'],
            (int) $_POST['bonus'],
            (float) $_POST['gift'],
            (string) $_POST['incoming_date']
        );
    }

    private function noSalaries(Entrepreneur $entrepreneur)
    {
        return $this->view->render('nosalaries', [
            'title' => 'Выписка ' . $entrepreneur->getFullname(),
            'description' => 'Нет дохода',
            'entrepreneur' => $entrepreneur
        ]);
    }
}