<?php

declare(strict_types=1);

namespace App;

class WebApp
{
    private Router $router;

    public function init()
    {
        $routes = require (__DIR__ . '/../routes.php');
        $this->router = new Router($routes);
        $this->router->run();
    }
}