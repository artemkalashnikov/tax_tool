<?php

declare(strict_types=1);

namespace App\Models;

use App\Collections\EntrepreneurCollection;
use App\Database;
use App\Model;

class Entrepreneur extends Model
{
    static private string $table = 'entrepreneur';

    private int $id;
    private string $name;
    private string $surname;
    private string $middleName;
    private string $email;

    public function __construct(int $id, string $surname, string $name, string $middleName, string $email)
    {
        $this->id = $id;
        $this->surname = $surname;
        $this->name = $name;
        $this->middleName = $middleName;
        $this->email = $email;
    }

    static public function find(int $id): Entrepreneur
    {
        $pdo = new Database();
        $table = self::$table;
        $sql = "SELECT * FROM {$table} WHERE id = ?";
        $stmt = $pdo->prepare($sql);

        $stmt->execute([$id]);
        $object = $stmt->fetch();
        $stmt->closeCursor();

        return new self(
            (int)$object['id'],
            $object['surname'],
            $object['name'],
            $object['middlename'],
            $object['email']
        );
    }

    public static function findAll(): ?EntrepreneurCollection
    {
        $pdo = new Database();
        $table = self::$table;
        $sql = "SELECT * FROM {$table}";

        $stmt = $pdo->query($sql);

        if (is_bool($stmt)) {
            return null;
        }

        $array = $stmt->fetchAll();

        if (empty($array)) {
            return null;
        }

        return new EntrepreneurCollection($array);
    }

    public function delete()
    {
        $pdo = new Database();
        $table = self::$table;
        $sql = "DELETE FROM {$table} WHERE id = ?";
        $stmt = $pdo->prepare($sql);

        $stmt->execute([$this->id]);
        $stmt->closeCursor();
    }

    public function save()
    {
        $pdo = new Database();
        $table = self::$table;

        if ($this->id !== 0) {
            $sql = "UPDATE {$table} SET id = ?, name = ?, surname = ?, middlename = ?, email = ? WHERE id = ?";
            $stmt = $pdo->prepare($sql);

            $values = [
                $this->id,
                $this->name,
                $this->surname,
                $this->middleName,
                $this->email,
                $this->id
            ];

            $stmt->execute($values);
            $stmt->closeCursor();
        } else {
            $sql = "INSERT INTO {$table} (name, surname, middlename, email) VALUES (?, ?, ?, ?)";
            $stmt = $pdo->prepare($sql);

            $values = [
                $this->name,
                $this->surname,
                $this->middleName,
                $this->email
            ];

            $stmt->execute($values);
            $stmt->closeCursor();
        }
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getSurname(): string
    {
        return $this->surname;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getMiddleName(): string
    {
        return $this->middleName;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getFullname(): string
    {
        return sprintf('%s %s %s', $this->surname, $this->name, $this->middleName);
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function setMiddleName(string $middleName): self
    {
        $this->middleName = $middleName;

        return $this;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function setFullname(string $surname, string $name, string $middlename): self
    {
        $this->surname = $surname;
        $this->name = $name;
        $this->middleName = $middlename;

        return $this;
    }
}