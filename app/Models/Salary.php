<?php

declare(strict_types=1);

namespace App\Models;

use App\Collections\SalaryCollection;
use App\Database;
use App\Model;
use DateTime;
use Exception;

class Salary extends Model
{
    static private string $table = 'salary';

    private int $id;
    private int $entrepreneurId;
    private float $USD;
    private int $bonus;
    private float $gift;
    private float $finalUSD;
    private float $finalUAH;
    private DateTime $incomingDate;

    public function __construct(int $id,
                                int $entrepreneurId,
                                float $USD,
                                float $finalUSD,
                                float $finalUAH,
                                int $bonus = 0,
                                float $gift = 0,
                                string $incomingDate = 'now'
                                )
    {
        $this->id = $id;
        $this->entrepreneurId = $entrepreneurId;
        $this->USD = $USD;
        $this->bonus = $bonus;
        $this->gift = $gift;
        $this->finalUSD = $finalUSD;
        $this->finalUAH = $finalUAH;

        try {
            $this->incomingDate = new DateTime($incomingDate);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    static public function find(int $id): Salary
    {
        $pdo = new Database();
        $table = self::$table;
        $sql = "SELECT * FROM {$table} WHERE id = ?";
        $stmt = $pdo->prepare($sql);

        $stmt->execute([$id]);
        $object = $stmt->fetch();
        $stmt->closeCursor();

        return new self(
            (int) $object['id'],
            (int) $object['entrepreneur_id'],
            (float) $object['usd'],
            (float) $object['final_usd'],
            (float) $object['final_uah'],
            (int) $object['bonus'],
            (float) $object['gift'],
            (string) $object['incoming_date'],
        );
    }

    public function save()
    {
        $pdo = new Database();
        $table = self::$table;

        if ($this->id !== 0) {
            $sql = "UPDATE {$table} SET id = ?,
                 entrepreneur_id = ?,
                 usd = ?,
                 bonus = ?,
                 gift = ?,
                 final_usd = ?,
                 final_uah = ?,
                 incoming_date = ? WHERE id = ?";
            $stmt = $pdo->prepare($sql);

            $values = [
                $this->id,
                $this->entrepreneurId,
                $this->USD,
                $this->bonus,
                $this->gift,
                $this->finalUSD,
                $this->finalUAH,
                $this->incomingDate,
                $this->id,
            ];

            $stmt->execute($values);
            $stmt->closeCursor();
        } else {
            $sql = "INSERT INTO {$table} (entrepreneur_id,
                   usd,
                   bonus,
                   gift,
                   final_usd,
                   final_uah,
                   incoming_date) VALUES (?, ?, ?, ?, ?, ?, ?)";
            $stmt = $pdo->prepare($sql);

            $values = [
                $this->entrepreneurId,
                $this->USD,
                $this->bonus,
                $this->gift,
                $this->finalUSD,
                $this->finalUAH,
                $this->incomingDate->format('Y-m-d'),
            ];

            $stmt->execute($values);
            $stmt->closeCursor();
        }
    }

    public function delete()
    {
        $pdo = new Database();
        $table = self::$table;
        $sql = "DELETE FROM {$table} WHERE id = ?";
        $stmt = $pdo->prepare($sql);

        $stmt->execute([$this->id]);
        $stmt->closeCursor();
    }

    public static function findAll(int $id): ?SalaryCollection
    {
        $pdo = new Database();
        $table = self::$table;
        $sql = "SELECT * FROM {$table} WHERE entrepreneur_id = ?";
        $stmt = $pdo->prepare($sql);

        if (is_bool($stmt)) {
            return null;
        }

        $stmt->execute([$id]);
        $array = $stmt->fetchAll();

        if (empty($array)) {
            return null;
        }

        return new SalaryCollection($array);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getEntrepreneurId(): int
    {
        return $this->entrepreneurId;
    }

    public function getUSD(): float
    {
        return $this->USD;
    }

    public function getBonus(): int
    {
        return $this->bonus;
    }

    public function getGift(): float
    {
        return $this->gift;
    }

    public function getFinalUSD(): float
    {
        return $this->finalUSD;
    }

    public function getFinalUAH(): float
    {
        return $this->finalUAH;
    }

    public function getIncomingDate(): string
    {
        return $this->incomingDate->format('d.m.Y');
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function setEntrepreneurId(int $entrepreneurId): self
    {
        $this->entrepreneurId = $entrepreneurId;

        return $this;
    }

    public function setUSD(float $USD): self
    {
        $this->USD = $USD;

        return $this;
    }

    public function setBonus(int $bonus): self
    {
        $this->bonus = $bonus;

        return $this;
    }

    public function setGift(float $gift): self
    {
        $this->gift = $gift;

        return $this;
    }

    public function setIncomingDate(string $incomingDate): self
    {
        try {
            $this->incomingDate = new \DateTime($incomingDate);
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return $this;
    }
}