<?php

declare(strict_types=1);

namespace App\DTO;

use DateTime;

class SalaryDTO
{
    private int $id;
    private int $entrepreneurId;
    private float $USD;
    private int $bonus;
    private float $gift;
    private string $incomingDate;

    public function __construct(int $id,
                                float $USD,
                                int $bonus,
                                float $gift,
                                string $incomingDate)
    {
        $this->id = $id;
        $this->USD = $USD;
        $this->bonus = $bonus;
        $this->gift = $gift;
        $this->incomingDate = $incomingDate;
    }

    public function setEntrepreneurId(int $entrepreneurId): void
    {
        $this->entrepreneurId = $entrepreneurId;
    }

    public function getEntrepreneurId(): int
    {
        return $this->entrepreneurId;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getUSD(): float
    {
        return $this->USD;
    }

    public function getBonus(): int
    {
        return $this->bonus;
    }

    public function getGift(): float
    {
        return $this->gift;
    }

    public function getIncomingDate(): string
    {
        return $this->incomingDate;
    }
}