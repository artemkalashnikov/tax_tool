<?php

declare(strict_types=1);

namespace App;

use App\View;

abstract class Controller
{
    protected $view;
    
    public function __construct()
    {
        $this->view = new View();
    }
}