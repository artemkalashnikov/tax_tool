<?php

declare(strict_types=1);

/**
 * MySQL connection array
 */
$_ENV['TAX_TOOL_MYSQL'] = [
    'host' => 'localhost',
    'dbname' => 'tax_tool',
    'user' => 'artem',
    'password' => '12345'
];

$_ENV['TAX_TOOL_REDIS'] = [
    'sceme' => 'tcp',
    'host' => '127.0.0.1',
    'port' => '6379'
];