<?php

declare(strict_types=1);

require (__DIR__ . '/vendor/autoload.php');
require (__DIR__ . '/config.php');
require (__DIR__ . '/helpers.php');

$app = new \App\WebApp();
$app->init();
